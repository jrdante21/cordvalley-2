<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Admin extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes;

    protected $hidden = ['password'];

    protected $appends = ['fullname', 'halfname', 'registered'];

    protected $fillable = ['fname', 'mname', 'lname', 'username', 'password', 'is_super'];

    protected $casts = [
        'is_super' => 'boolean'
    ];

    public function getFullnameAttribute()
    {
        return Str::title(trim("{$this->fname} {$this->mname} {$this->lname} {$this->suffix}"));
    }

    public function getHalfnameAttribute()
    {
        return trim("{$this->fname} {$this->lname}");
    }
    
    public function getRegisteredAttribute()
    {
        return date('M d, Y h:m a', strtotime($this->created_at));
    }

    public function getModifiedAttribute()
    {
        return date('M d, Y h:m a', strtotime($this->updated_at));
    }

    //// Scopes

    // Search by name
    public function scopeSearchByName($query, $search='')
    {
        if (empty($search)) return $query;
        
        $search = '%'.$search.'%';
        return $query
        ->whereRaw("(
            CONCAT(members.fname, ' ', members.mname, ' ', members.lname) LIKE ? OR
            CONCAT(members.fname, ' ', members.lname) LIKE ? OR
            CONCAT(members.lname, ' ', members.fname) LIKE ? OR
            members.username LIKE ?
        )", [$search, $search, $search, $search]);
    }
}
