<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MemberPayoutItem extends Model
{
    use HasFactory;

    protected $fillable = ['member_network_point_id','binary_points','sponsor_points','ordinal_points','purchase_points'];
    
    public $timestamps = false;
}
