<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CodeItem extends Model
{
    use HasFactory;

    protected $fillable = ['code','member_network_id'];

    protected $appends = ['used'];

    public $timestamps = false;
    
    public function getUsedAttribute()
    {
        if (empty($this->used_at)) return 'None';
        return date('M d, Y h:m a', strtotime($this->used_at));
    }

    //// Relationships
    public function remarks()
    {
        return $this->belongsTo(Code::class, 'code_id', 'id');
    }

    public function admin()
    {
        return $this->hasOneThrough(Admin::class, Code::class, 'id', 'id', 'code_id', 'admin_id');
    }

    //// Scopes
    
    public function scopeSelectNetworkDetails($query)
    {
        return $query
            ->selectRaw("
                code_items.id,
                code_items.code,
                code_items.code_id,
                code_items.member_network_id,
                code_items.used_at,
                member_networks.username AS networkUsername,
                CONCAT(members.fname, ' ', IFNULL(members.mname, ''), ' ', members.lname, ' ', IFNULL(members.suffix, '') ) AS memberFullname
            ")
            ->leftJoin('member_networks', 'code_items.member_network_id', '=', 'member_networks.id')
            ->leftJoin('members', 'member_networks.member_id', '=', 'members.id')
            ;
    }
}
