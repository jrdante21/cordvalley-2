<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Purchase extends Model
{
    use HasFactory;

    protected $fillable = ['member_network_id','admin_id','remarks','ar_number','purchased_at'];

    protected $appends = ['created','purchased'];

    public function getCreatedAttribute()
    {
        return date('M d, Y h:m:s a', strtotime($this->created_at));
    }

    public function getPurchasedAttribute()
    {
        return date('M d, Y', strtotime($this->purchased_at));
    }

    /// Relations

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function items()
    {
        return $this->hasMany(PurchaseItem::class)->with('product');
    }

    /// Scopes

    public function scopeWithMemberDetails($query)
    {
        return $query->leftJoin('view_member_network_purchases', 'purchases.id', '=', 'view_member_network_purchases.purchase_id');
    }

    // must include scopeWithMemberDetails
    public function scopeSearchUsingView($query, $search)
    {
        $search = "%".$search."%";
        return $query->whereRaw("(
            purchases.ar_number LIKE ? OR
            purchases.remarks LIKE ? OR
            network_username LIKE ? OR
            member_fullname LIKE ?
        )", [$search, $search, $search, $search]);
    }
}
