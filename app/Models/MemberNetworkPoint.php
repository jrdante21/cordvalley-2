<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MemberNetworkPoint extends Model
{
    use HasFactory;

    protected $fillable = [
        'member_network_id','lft_count','rgt_count','pair_count','sponsor_count',
        'binary_points','sponsor_points','purchase_points','ordinal_points',
        'binary_points_payout','sponsor_points_payout','purchase_points_payout','ordinal_points_payout',
        'cycle','type','date'];

    public $timestamps = false;

    /// Scopes

    public function scopeWhereDateCycle($query, $date, $cycle, $type = 1, $operator = '=')
    {
        return $query->whereRaw("CONCAT(member_network_points.date, '-0', member_network_points.cycle) {$operator} ? AND type = ?", [$date.'-0'.$cycle, $type]);
    }

    public function scopeOrderByDateCycle($query, $dir = 'DESC')
    {
        return $query->orderByRaw("CONCAT(member_network_points.date, '-0', member_network_points.cycle) {$dir}");
    }

    public function scopeWithProfitDetails($query)
    {
        return $query->leftJoin('view_member_point_profits', 'member_network_points.id', '=', 'view_member_point_profits.point_profit_id');
    }
}
