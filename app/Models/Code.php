<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    use HasFactory;

    protected $appends = ['created'];

    public function getCreatedAttribute()
    {
        return date('M d, Y h:m:s a', strtotime($this->created_at));
    }

    //// Relationships
    public function items()
    {
        return $this->hasMany(CodeItem::class)->selectNetworkDetails();
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
}
