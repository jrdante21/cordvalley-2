<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;

    public function scopeGetAddress($query)
    {
        $settings = $query->where('keyname', 'address')->first();

        $addr = [];
        foreach (json_decode($settings->keyvalue, true) as $v) {
            $addr[] = $v['province_list'];
        }
        return collect($addr)->collapse()->all();
    }
}
