<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MemberNetwork extends Model
{
    use HasFactory;

    protected $fillable = ['member_id','username','placed_in','sponsored_by','lft_leg','rgt_leg','binary_nodes','sponsor_nodes','type'];

    protected $appends = ['binary_nodes_array','sponsor_nodes_array','uplines_array','cycle','registered_date','registered','modified'];

    public function getCycleAttribute()
    {
        $time = (int) date('G', strtotime($this->created_at));
        return $time < 12 ? 1 : 2;
    }

    public function getRegisteredDateAttribute()
    {
        return date('Y-m-d', strtotime($this->created_at));
    }

    public function getRegisteredAttribute()
    {
        return date('M d, Y h:m a', strtotime($this->created_at));
    }

    public function getModifiedAttribute()
    {
        return date('M d, Y h:m a', strtotime($this->updated_at));
    }

    public function getBinaryNodesArrayAttribute()
    {
        $nodes = explode('.', $this->binary_nodes);
        return collect($nodes)->filter()->values()->all();
    }

    public function getSponsorNodesArrayAttribute()
    {
        $nodes = explode('.', $this->sponsor_nodes);
        return collect($nodes)->filter()->values()->all();
    }

    public function getUplinesArrayAttribute()
    {
        $nodes1 = explode('.', $this->binary_nodes);
        $nodes2 = explode('.', $this->sponsor_nodes);
        return collect($nodes1)->concat($nodes2)->unique()->values()->all();
    }

    //// Relationships

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public function code()
    {
        return $this->hasOne(CodeItem::class);
    }

    //// Scopes

    public function scopeWhereDateCycle($query, $date, $cycle)
    {
        return $query
            ->whereDate('member_networks.created_at', $date)
            ->whereRaw("(CASE WHEN DATE_FORMAT(member_networks.created_at, '%k') < 12 THEN 1 ELSE 2 END) = ?", [$cycle])
            ;
    }

    public function scopeWhereByUplines($query, $uplines)
    {
        if (!is_array($uplines)) $uplines = [ $uplines ];
        return $query->where(function($query) use ($uplines) {
            foreach ($uplines as $id) {
                $like = "%.{$id}.%";
                $query->orWhere('member_networks.binary_nodes', 'LIKE', $like);
                $query->orWhere('member_networks.sponsored_by', '=', $id);
            }
            return $query;
        });
    }

    public function scopeWithCheckStatus($query, $date = '', $min_purchased = 500)
    {
        if (empty($date)) $date = date('Y-m-d');

        $month_year = date('Y-m', strtotime('-1 months', strtotime($date)));
        $date_diff = "DATEDIFF('$date', member_networks.created_at)";
        return $query
            ->leftJoin(
                DB::raw("(
                    SELECT
                        member_networks.id AS member_network_id,
                        purchased.purchased_total,
                        purchased_total_qty,
                        {$date_diff} AS date_diff,
                        (CASE WHEN purchased.purchased_total >= {$min_purchased} OR {$date_diff} <= 30 THEN 1 ELSE 0 END) AS status
                    FROM member_networks
	                LEFT JOIN (
                        SELECT
                            member_network_id,
                            SUM(purchased_total) AS purchased_total,
                            SUM(quantity) AS purchased_total_qty
                        FROM 
                            view_member_network_purchase_items
                        WHERE DATE_FORMAT(purchased_at, '%Y-%m') >= '{$month_year}'
                        GROUP BY member_network_id
                    ) AS purchased ON member_networks.id = purchased.member_network_id
                ) AS purchased"),
                'member_networks.id', '=', 'purchased.member_network_id'
            );
    }

    public function scopeWithGenealogyDetails($query)
    {
        return $query->leftJoin('view_member_network_gene_details', 'member_networks.id', '=', 'view_member_network_gene_details.member_network_id');
    }

    public function scopeWithLatestLegCount($query, $date = '', $cycle = 1, $type = 1)
    {
        if (empty($date)) $date = date('Y-m-d');

        $date_cycle = $date.'-0'.$cycle;
        $date_cycle_field = "CONCAT(date,'-0',cycle)";
        return $query
            ->leftJoin(
                DB::raw("(
                    SELECT
                        member_network_id,
                        id AS id_count_prev,
                        lft_count AS lft_count_prev,
                        rgt_count AS rgt_count_prev
                    FROM member_network_points AS p1
                    WHERE id = (
                            SELECT id 
                            FROM member_network_points AS p2 
                            WHERE 
                                p2.member_network_id = p1.member_network_id AND
                                CONCAT(p2.date,'-0',p2.cycle) < '{$date_cycle}' AND
                                p2.type = {$type}
                            ORDER BY CONCAT(p2.date,'-0',p2.cycle)
                            DESC LIMIT 1
                        )
                ) AS prev_leg_count"), 'member_networks.id', '=', 'prev_leg_count.member_network_id'
            )
            ->leftJoin(
                DB::raw("(
                    SELECT
                        member_network_id,
                        id AS id_count_curr,
                        lft_count AS lft_count_curr,
                        rgt_count AS rgt_count_curr
                    FROM member_network_points
                    WHERE {$date_cycle_field} = '{$date_cycle}' AND type = {$type}
                ) AS curr_leg_count"), 'member_networks.id', '=', 'curr_leg_count.member_network_id'
            );
    }

    // get the total purchase and points in a date
    public function scopeWithPurchaseTotalDate($query, $date)
    {
        $date = date('Y-m-d', strtotime($date));
        return $query
            ->leftJoin(DB::raw("(
                SELECT
                    purchased.member_network_id,
                    purchased.purchase_points_total
                FROM member_networks
                LEFT JOIN (
                    SELECT
                        member_network_id,
                        SUM(points_total) AS purchase_points_total
                    FROM 
                        view_member_network_purchase_items
                    WHERE DATE(purchased_at) = '{$date}'
                    GROUP BY member_network_id
                ) AS purchased ON member_networks.id = purchased.member_network_id
            ) AS purchased_points"),
            'member_networks.id', '=', 'purchased_points.member_network_id'
        );
    }

    // Get Profits
    public function scopeWithCurrentProfits($query)
    {
        return $query
            ->leftJoin(DB::raw("(
                SELECT
                    member_network_id,
                    SUM(binary_points + sponsor_points + ordinal_points + purchase_points) AS profit_total
                FROM member_network_points
                GROUP BY member_network_id
            ) AS profits"),
            'member_networks.id', '=', 'profits.member_network_id')
            ;
    }
}
