<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurchaseItem extends Model
{
    use HasFactory;

    protected $fillable = ['product_id','price','points','quantity'];

    protected $appends = ['total_amount','total_points'];

    public $timestamps = false;

    public function getTotalAmountAttribute()
    {
        return $this->price * $this->quantity;
    }

    public function getTotalPointsAttribute()
    {
        return $this->points * $this->quantity;
    }

    /// Relations

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /// Scopes

}
