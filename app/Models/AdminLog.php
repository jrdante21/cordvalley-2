<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminLog extends Model
{
    use HasFactory;

    protected $fillable = ['admin_id', 'keyword', 'keyword_id', 'previous_data'];

    public function scopeCreateLog($query, $keyword, $keyword_id, $data)
    {
        return $query->create([
            'admin_id' => auth()->guard('admin')->id(),
            'keyword' => $keyword,
            'keyword_id' => $keyword_id,
            'previous_data' => json_encode($data)
        ]);
    }
}
