<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class Member extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $hidden = [ 'password' ];

    protected $fillable = [ 'account_id', 'fname','mname','lname','suffix','gender','bday','contact_number', 'barangay','municipality','province', 'username','password' ];

    protected $appends = ['fullname','address','gender_str','bday_str','registered','modified'];

    public function getFullnameAttribute()
    {
        return Str::title(trim("{$this->fname} {$this->mname} {$this->lname} {$this->suffix}"));
    }

    public function getAddressAttribute()
    {
        $address = collect([$this->barangay, $this->municipality, $this->province])->filter()->join(', ');
        if (empty($address)) return 'N/A';
        return Str::title($address);
    }

    public function getGenderStrAttribute()
    {
        return ($this->gender <= 1) ? 'Male' : 'Female';
    }

    public function getBdayStrAttribute()
    {
        return date('M d, Y', strtotime($this->bday));
    }

    public function getRegisteredAttribute()
    {
        return date('M d, Y h:m a', strtotime($this->created_at));
    }

    public function getModifiedAttribute()
    {
        return date('M d, Y h:m a', strtotime($this->updated_at));
    }

    /// Relations
    public function networks()
    {
        return $this->hasMany(MemberNetwork::class);
    }

    /// Scopes

    // Add where for address
    public function scopeWhereAddress($query, $request)
    {
        return $query
        ->when($request->province, fn($query, $province) => $query->where('province', $province))
        ->when($request->municipality, fn($query, $municipality) => $query->where('municipality', $municipality))
        ->when($request->barangay, fn($query, $barangay) => $query->where('barangay', $barangay));
    }

    // Find by username
    public function scopeFindByUsername($query, $username='')
    {
        return $query->where('account_id', $account)->orWhere('username', $username);
    }
    
    // Search by name
    public function scopeSearchByName($query, $search='')
    {
        if (empty($search)) return $query;
        
        $search = '%'.$search.'%';
        return $query
        ->whereRaw("(
            CONCAT(members.fname, ' ', members.mname, ' ', members.lname) LIKE ? OR
            CONCAT(members.fname, ' ', members.lname) LIKE ? OR
            CONCAT(members.lname, ' ', members.fname) LIKE ? OR
            members.username LIKE ?
        )", [$search, $search, $search, $search]);
    }

    // Generate Account id
    public function scopeGenerateAccountID($query)
    {
        $account_id = 0;
        do {
            $id = mt_rand(1000000, 9999999);
            $count = $query->where('account_id', $id)->count();
            if ($count <= 0) $account_id = $id;

        } while ($account_id <= 0);

        return $account_id;
    }

    public function scopeWithTotalProfit($query)
    {
        return $query
            ->leftJoin(DB::raw("(
                SELECT
                    member_id,
                    GROUP_CONCAT(DISTINCT point_profit_id SEPARATOR ',') AS point_profit_ids,
                    IFNULL(SUM(total_profit), 0) AS total_profit,
                    SUM(total_payout) AS total_payout
                FROM view_member_point_profits
                WHERE total_profit != total_payout
                GROUP BY member_id
            ) AS profit"), 'members.id', '=', 'profit.member_id');
    }

    public function scopeWithTotalOverallProfit($query)
    {
        return $query
        ->leftJoin(DB::raw("(
            SELECT
                member_id,
                SUM(IFNULL(total_profit,0) - IFNULL(total_payout,0)) AS total_balance,
                SUM(total_profit) AS total_profit,
                SUM(total_payout) AS total_payout
            FROM view_member_point_profits
            GROUP BY member_id
        ) AS profit"), 'members.id', '=', 'profit.member_id');
    }

    public function scopeWithTotalNetworkings($query)
    {
        return $query
            ->leftJoin(DB::raw("(
                SELECT
                    member_id,
                    COUNT(*) AS networking_count
                FROM member_networks
                GROUP BY member_id
            ) AS networkings"), 'members.id', '=', 'networkings.member_id');
    }
}
