<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['admin_id','title','abbreviation','description','price','price_discounted','points'];

    protected $appends = ['created'];

    public function getCreatedAttribute()
    {
        return date('M d, Y h:m:s a', strtotime($this->created_at));
    }

    /// Relations

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

}
