<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MemberPayout extends Model
{
    use HasFactory;

    protected $fillable = ['member_id','remarks','admin_id','confirmed_at','requested_at'];

    protected $appends = ['confirmed','requested'];

    public function getConfirmedAttribute()
    {
        return date('M d, Y', strtotime($this->confirmed_at));
    }

    public function getRequestedAttribute()
    {
        return date('M d, Y', strtotime($this->requested_at));
    }
    
    /// Relations

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function items()
    {
        return $this->hasMany(MemberPayoutItem::class);
    }

    /// Scopes

    public function scopeConfirmed($query)
    {
        return $query->whereNotNull('confirmed_at');
    }

    public function scopeWithTotalProfit($query)
    {
        return $query
            ->leftJoin(DB::raw("(
                SELECT
                    member_payout_id,
                    SUM(total_payout) AS total_profit
                FROM view_member_payout_items
                GROUP BY member_payout_id
            ) AS profit"), 'member_payouts.id', '=', 'profit.member_payout_id'
            );
    }
}
