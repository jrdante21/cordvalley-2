<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use App\Models\MemberPayout;

class MemberProfitSummaryHelper
{
    public $member_id = 0;

    public function __construct($member_id)
    {
        $this->member_id = $member_id;
    }

    public function summarize($page = 20)
    {
        $member_id = $this->member_id;

        $selectTotalPayout = "SELECT SUM(total_payout) FROM view_member_payout_items AS payout WHERE member_id = {$member_id}";
        $selectTotalProfit = "SELECT SUM(total_profit) FROM view_member_point_profits AS profit WHERE member_id = {$member_id}";

        $payouts = MemberPayout::select(DB::raw("
                'payout' AS type,
                remarks,
                CONCAT(confirmed_at, '-02') COLLATE utf8mb4_general_ci AS date_cycle,
                profit.total_profit AS current_amount,
                IFNULL(({$selectTotalPayout} AND payout.confirmed_at <= member_payouts.confirmed_at), 0) AS total_payout,
                IFNULL(({$selectTotalProfit} AND profit.date_cycle <= CONCAT(member_payouts.confirmed_at, '-02') COLLATE utf8mb4_general_ci), 0) AS total_profit
            "))
            ->withTotalProfit()
            ->where('member_payouts.member_id', $member_id);

        $result = DB::table('view_member_point_profits AS view_profit')
            ->select(DB::raw("
                'profit' AS type,
                JSON_OBJECT(
                    'username', username,
                    'binary_points', binary_points,
                    'sponsor_points', sponsor_points,
                    'purchase_points', purchase_points
                ) AS remarks,
                date_cycle,
                total_profit AS current_amount,
                IFNULL(({$selectTotalPayout} AND payout.date_cycle <= view_profit.date_cycle), 0) AS total_payout,
                IFNULL(({$selectTotalProfit} AND profit.date_cycle <= view_profit.date_cycle), 0) AS total_profit
            "))
            ->leftJoin('member_networks', 'view_profit.member_network_id', '=', 'member_networks.id')
            ->where('view_profit.member_id', $member_id)
            ->havingRaw('current_amount >= 1')
            ->unionAll($payouts)
            ->orderBy('date_cycle', 'desc')
            ->paginate($page);

            $result->getCollection()->transform(function ($item) {
                // current profit
                $item->current_profit = $item->total_profit - $item->total_payout;

                // Add positive|negative for amounts
                $item->current_amount = $item->type == 'profit'
                    ? '+'.$item->current_amount
                    : '-'.$item->current_amount;

                // remarks for profit
                if ($item->type == 'profit') {
                    $details = json_decode($item->remarks);
                    $item->remarks = [ $details->username ];
                    if ($details->binary_points >= 1) $item->remarks[] = "Pairing Points: " . $details->binary_points;
                    if ($details->sponsor_points >= 1) $item->remarks[] = "Sponsor Points: " . $details->sponsor_points;
                    if ($details->purchase_points >= 1) $item->remarks[] = "Purchase Points: " . $details->purchase_points;

                    $item->remarks = implode("<br/>", $item->remarks);
                }
                return $item;
            });

        return $result;
    }
}