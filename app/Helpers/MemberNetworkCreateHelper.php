<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use App\Models\MemberNetwork;
use App\Models\CodeItem;
use App\Helpers\MemberNetworkComputeHelper;

class MemberNetworkCreateHelper 
{
    public Request $request;

    public $errors = array();

    public $member_network_items = array(
        'type' => 1,
        'sponsor_nodes' => '.',
        'binary_nodes' => '.',
    );

    public $member_id = null;

    public $member_network_id = null;

    public $code_id = null;

    public $no_code = false;

    public $placed_in = null;

    public $datetime = null;

    public $placement = '';

    public function __construct(Request $request, $member_id) {
        $this->request = $request;
        $this->member_id = $member_id;
    }

    // Validate all requirements
    public function validate()
    {
        if (!$this->no_code) $this->__getCode();
        $this->__getSponsor();
        $this->__getPlacement();
    }

    public function save()
    {
        $computeNetwork = new MemberNetworkComputeHelper();

        $this->__saveMemberNetwork();
        $this->__savePlacement();
        if (!$this->no_code) $this->__saveCode();

        $computeNetwork->computeUplines($this->member_network_id);
    }

    //// Private functions

    private function __getCode()
    {
        $code = CodeItem::where('code', $this->request->code)->first();
        if (empty($code)) {
            $this->errors['code'] = 'Code not found!';

        } else {
            // Check if code is already used
            if (!empty($code->member_network_id)) {
                $this->errors['code'] = 'Code already used!';

            } else {
                $this->code_id = $code->id;
            }
        }
    }

    private function __getSponsor()
    {
        if(!empty($this->request->no_sponsor)) return;

        $sponsor = MemberNetwork::where('username', $this->request->sponsored_by)->first();
        if (empty($sponsor)) {
            $this->errors['sponsored_by'] = "Sponsor's username not found!";

        } else {
            $this->member_network_items['sponsored_by'] = $sponsor->id;

            // Setup sponsor nodes
            $nodes = (empty($sponsor->sponsor_nodes)) ? '.' : $sponsor->sponsor_nodes;
            $this->member_network_items['sponsor_nodes'] = $nodes . $sponsor->id . '.';
        }
    }

    private function __getPlacement()
    {
        if(!empty($this->request->no_sponsor)) return;

        $placement = MemberNetwork::where('username', $this->request->placed_in)->first();

        if (empty($placement)) {
            $this->errors['placed_in'] = "Placement's username not found!";

        } else {
            // Check if legs already used
            $leg = ($this->request->placement <= 1) ? 'lft_leg' : 'rgt_leg';
            if (!empty($placement->$leg)) {
                $this->errors['placement'] = "Placement already occupied!";
                return;
            }

            // Setup binary nodes
            $nodes = (empty($placement->binary_nodes)) ? '.' : $placement->binary_nodes;
            $this->member_network_items['binary_nodes'] = $nodes . $placement->id . '.';
            $this->member_network_items['placed_in'] = $placement->id;
            $this->placed_in = $placement->id;
            $this->placement = $leg;
        }
    }

    //// Private functions saver

    private function __saveMemberNetwork()
    {
        $items = $this->member_network_items;
        $items['member_id'] = $this->member_id;
        $items['username'] = $this->request->username;

        $network = MemberNetwork::create($items);
        $this->member_network_id = $network->id;
        $this->datetime = $network->created_at;
    }

    private function __savePlacement()
    {
        if(!empty($this->request->no_sponsor)) return;

        $items[$this->placement] = $this->member_network_id;
        MemberNetwork::where('id', $this->placed_in)->update($items);
    }

    private function __saveCode()
    {
        CodeItem::where('id', $this->code_id)->update([
            'member_network_id' => $this->member_network_id,
            'used_at' => $this->datetime,
        ]);
    }
}