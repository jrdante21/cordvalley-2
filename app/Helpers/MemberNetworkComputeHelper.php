<?php

namespace App\Helpers;

use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use App\Models\MemberNetwork;
use App\Models\MemberNetworkPoint;

class MemberNetworkComputeHelper 
{
    private $binary_points = 250;

    private $sponsor_points = 250;

    private $pair_limit = 10;

    public $date = null;
     
    public $cycle = null;

    public $type = 1;

    public function computeUplines($member_id)
    {
        $member = MemberNetwork::find($member_id);
        if (empty($member)) return null;

        // setup date and cycle
        $this->date = $member->registered_date;
        $this->cycle = $member->cycle;

        // Extract uplines
        $uplines = $member->uplines_array;
        if (empty($uplines)) return null;

        $uplines = $this->__saveNetworkingPoints($uplines);

        return $uplines;
    }

    /// Private function ------

    // Takes an array of ids
    private function __saveNetworkingPoints($uplines) // array
    {
        $resultUplines = MemberNetwork::withLatestLegCount($this->date, $this->cycle, $this->type)
            //->withCheckStatus($this->date)
            ->whereIn('member_networks.id', $uplines)
            ->get();            

        // Get downlines 
        $downlines = MemberNetwork::whereDateCycle($this->date, $this->cycle)->whereByUplines($uplines)->get();

        // get each uplines left and right legs
        $pointsToUpserts = [];
        foreach ($resultUplines as $item) {
            $lft_id = $item->lft_leg;
            $rgt_id = $item->rgt_leg;

            // compute pairings
            $value = $this->__computePairings(
                // get previous counts
                $item->lft_count_prev,
                $item->rgt_count_prev,

                // count current legs from downlines
                $downlines->filter(fn($value) => in_array($lft_id, $value->binary_nodes_array) || $lft_id == $value->id)->count(),
                $downlines->filter(fn($value) => in_array($rgt_id, $value->binary_nodes_array) || $rgt_id == $value->id)->count()
            );

            // Append in values
            $value['id'] = $item->id_count_curr;
            $value['member_network_id'] = $item->id;
            $value['date'] = $this->date;
            $value['cycle'] = $this->cycle;
            $value['type'] = $this->type;

            // Count sponsored downlines
            $value['sponsor_count'] = $downlines->where('sponsored_by', $item->id)->count();
            $value['sponsor_points'] = $this->sponsor_points * $value['sponsor_count'];

            $pointsToUpserts[] = $value;
        }

        // Upsert
        MemberNetworkPoint::upsert($pointsToUpserts, ['id']);
        return $pointsToUpserts;
    }

    /// Changed to public
    // under function __getDownlineCountPoints
    public function __computePairings($lft_count_prev, $rgt_count_prev, $lft_count_curr, $rgt_count_curr)
    {
        $value = [];

        // count current legs from downlines and previous counts
        $lft_count_curr = $lft_count_prev + $lft_count_curr;
        $rgt_count_curr = $rgt_count_prev + $rgt_count_curr;
        $value['lft_count'] = $lft_count_curr;
        $value['rgt_count'] = $rgt_count_curr;

        // get the lowest legs with previous and latest
        $prev_count_min = min($lft_count_prev, $rgt_count_prev);
        $curr_count_min = min($lft_count_curr, $rgt_count_curr);

        // Subtract the lowest to get the paired counts
        $value['pair_count'] = abs($prev_count_min - $curr_count_min);
        $value['binary_points'] = $this->binary_points * min($value['pair_count'], $this->pair_limit);

        return $value;
    }
}