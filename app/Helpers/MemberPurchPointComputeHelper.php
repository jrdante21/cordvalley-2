<?php

namespace App\Helpers;

use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use App\Models\MemberNetwork;
use App\Models\MemberNetworkPoint;

// Update member purchase points
// always on cycle 1
class MemberPurchPointComputeHelper
{
    public $date;

    public $type = 1;

    public $cycle = 1;

    public function __construct($date, $member_network_id)
    {
        $this->date = $date;
        $this->member_network_id = $member_network_id;
    }

    public function save()
    {
        $MemberNetwork = MemberNetwork::withLatestLegCount($this->date, $this->cycle, $this->type)
            ->withPurchaseTotalDate($this->date)
            ->findOrFail($this->member_network_id);

        $point_id = $MemberNetwork['id_count_curr'];
        if ($point_id >= 1) {
            MemberNetworkPoint::where('id', $point_id)
                ->update(['purchase_points' => $MemberNetwork['purchase_points_total']]);

        } else {
            MemberNetworkPoint::create([
                'member_network_id' => $this->member_network_id,
                'lft_count' => $MemberNetwork['lft_count_prev'] ?? 0,
                'rgt_count' => $MemberNetwork['rgt_count_prev'] ?? 0,
                'purchase_points' => $MemberNetwork['purchase_points_total'],
                'cycle' => $this->cycle,
                'type' => $this->type,
                'date' => $this->date,
            ]);
        }
    }

    // check purchase points is already payouted
    public function isPayouted()
    {
        $MemberNetworkPoint = MemberNetworkPoint::where('member_network_id', $this->member_network_id)
            ->whereDateCycle($this->date, $this->cycle, $this->type)
            ->first();
        
        if (empty($MemberNetworkPoint)) return false;

        return $MemberNetworkPoint->purchase_points_payout >= 1;
    }
}