<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\Models\MemberNetwork;

class SearchMembersNetworkController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $search = '%'.$request->input('search', '').'%';

        $result = MemberNetwork::selectRaw("
            member_networks.id,
            member_networks.username,
            CONCAT(members.fname, ' ', IFNULL(members.mname, ''), ' ', members.lname, ' ', IFNULL(members.suffix, '') ) AS fullname
        ")
        ->leftJoin('members', 'member_networks.member_id', '=', 'members.id')
        ->whereRaw("(
            CONCAT(members.fname, ' ', members.mname, ' ', members.lname) LIKE ? OR
            CONCAT(members.fname, ' ', members.lname) LIKE ? OR
            CONCAT(members.lname, ' ', members.fname) LIKE ? OR
            members.username LIKE ?
        )", [$search, $search, $search, $search])
        ->limit(20)
        ->get();

        return $result;
    }
}
