<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;

use App\Models\Member;
use App\Models\MemberPayout;

class MemberPayoutController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke($id, Request $request)
    {
        $member = Member::withTotalProfit()->findOrFail($id);

        $result = MemberPayout::with(['member'])
            ->withTotalProfit()
            ->when($request->search, fn($query, $search) => $query->where('remarks', 'LIKE', '%'.$search.'%'))
            ->when($request->pending,
                fn($query) => $query->whereNull('confirmed_at'),
                fn($query) => $query->with('admin')->confirmed()
            )
            ->orderBy('updated_at', 'desc')
            ->paginate($request->input('per_page', 20));

        return Inertia::render('Admin/Members/MemberPayouts', [
            'member' => $member,
            'result' => $result,
            'query' => $request->all()
        ]);
    }
}
