<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Helpers\MemberProfitSummaryHelper;
use App\Models\Member;

class MemberSummaryController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, $id)
    {
        $member = Member::findOrFail($id);
        $result = new MemberProfitSummaryHelper($id);
        $result = $result->summarize($request->input('per_page', 20));

        return Inertia::render('Admin/Members/MemberSummary', [
            'member' => $member,
            'result' => $result,
            'query' => $request->all()
        ]);
    }
}
