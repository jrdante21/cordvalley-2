<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\MemberNetworkRequest;
use Inertia\Inertia;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

use App\Models\Member;
use App\Models\MemberNetwork;
use App\Models\AdminLog;
use App\Helpers\MemberNetworkCreateHelper;

class MemberNetworkController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MemberNetworkRequest $request)
    {
        $valid = $request->validated();

        $memberNetworkHelper = new MemberNetworkCreateHelper($request, $valid['member_id']);
        $memberNetworkHelper->validate();

        if (!empty($memberNetworkHelper->errors)) return back()->withErrors($memberNetworkHelper->errors);
        
        DB::beginTransaction();
        try {
            $memberNetworkHelper->save();
            AdminLog::createLog('member_network_added', $memberNetworkHelper->member_network_id, []);
            
            DB::commit();
            return back();

        } catch (Throwable $e) {
            DB::rollback();
            back()->withErrors([ 'message' => "Something went wrong. Please try again.", 'ex' => $e ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $member, $id)
    {
        $valid = $request->validate([
            'username' => ['required', 'alpha_num', 'min:5', Rule::unique('member_networks', 'username')->ignore($id)],
        ]);

        $member_network = MemberNetwork::findOrFail($id);

        DB::beginTransaction();
        try {
            $member_network->update($valid);
            AdminLog::createLog('member_network_updated', $id, $member_network);
            DB::commit();
            return back();

        } catch (Throwable $e) {
            DB::rollback();
            back()->withErrors([ 'message' => "Something went wrong. Please try again.", 'ex' => $e ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
