<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Code;
use App\Models\CodeItem;
use Inertia\Inertia;
use Illuminate\Support\Facades\DB;

class CodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $result = CodeItem::with(['remarks','admin'])
            ->selectNetworkDetails()
            ->orderBy('code_items.code_id', 'DESC')
            ->when($request->search, function($query, $search){
                $query->where('code', 'like', '%'.$search.'%');
            })
            ->paginate($request->input('per_page', 20));

        return Inertia::render('Admin/Codes/Index', [
            'result' => $result,
            'query' => $request->all(),
            'codes' => $request->session()->get('codes')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valid = $request->validate([
            'code_number' => 'required|numeric|min:1|max:100',
            'type' => 'required|numeric|min:1|max:100',
            'remarks' => 'required|min:5'
        ]);

        // Generates codes
        $codes = null;
        do {
            $array = [];

            // generate code according to code_number 
            for ($i=1; $i <= $valid['code_number']; $i++) {
                $string = collect(str_split('QWERTYUIOPASDFGHJKLZXCVBNM1234567890'))->shuffle()->take(10)->all();
                $array[] = implode($string);
            }

            // Check if code is exists
            $count = CodeItem::whereIn('code', $array)->count();
            if ($count <= 0) $codes = $array;

        } while (empty($codes));

        // Save codes 
        $codesToSave = collect($codes)->map(fn($item) => ['code' => $item])->all();

        try {
            DB::beginTransaction();
            $code = new Code;
            $code->admin_id = auth()->guard('admin')->id();
            $code->remarks = $valid['remarks'];
            $code->save();
            $code->items()->createMany($codesToSave);
            DB::commit();
            return back()->with([ 'codes' => $codes ]);

        } catch (Throwable $e) {
            DB::rollback();
            return back()->withErrors([ 'message' => $e ]);
        }
    }
}
