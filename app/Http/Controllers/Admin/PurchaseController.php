<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\DB;

use App\Helpers\MemberPurchPointComputeHelper;
use App\Http\Requests\PurchaseRequest;
use App\Models\Purchase;
use App\Models\PurchaseItem;
use App\Models\MemberNetwork;
use App\Models\AdminLog;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $result = Purchase::with(['admin','items'])
            ->withMemberDetails()
            ->when($request->search, fn($query, $search) => $query->searchUsingView($search))
            ->orderBy('purchases.created_at', 'desc')
            ->paginate($request->input('per_page', 20));

        // Add urls
        $result->getCollection()->transform(function ($item) {
            $item->total_amount = collect($item->items)->sum(fn($i) => $i->total_amount);
            $item->total_points = collect($item->items)->sum(fn($i) => $i->total_points);
            return $item;
        });

        return Inertia::render('Admin/Purchases/Index', [
            'result' => $result,
            'query' => $request->all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PurchaseRequest $request)
    {
        $valid = $request->validated();
        $valid['admin_id'] = auth()->guard('admin')->id();

        // Purchase items
        $items = $valid['items'];
        unset($valid['items']);

        $purchPointCompute = new MemberPurchPointComputeHelper($valid['purchased_at'], $valid['member_network_id']);

        DB::beginTransaction();
        try {
            $purchase = Purchase::create($valid);
            $purchase->items()->createMany($items);

            // update purchase points
            $purchPointCompute->save();

            DB::commit();
            return back();

        } catch (Throwable $e) {
            DB::rollback();
            return back()->withErrors(['message' => $e]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PurchaseRequest $request, $id)
    {
        $valid = $request->validated();
        $purchase = Purchase::withMemberDetails()->findOrFail($id);

        // Purchase items
        $items = $valid['items'];
        unset($valid['items']);

        $computePrevious = new MemberPurchPointComputeHelper($purchase->purchased_at, $purchase->member_network_id);
        // return error if payouted
        if ($computePrevious->isPayouted()) return back()->withErrors(['message' => "Points from this purchase already payouted. Cannot be updated."]);

        $computeCurrent = new MemberPurchPointComputeHelper($valid['purchased_at'], $valid['member_network_id']);
        

        DB::beginTransaction();
        try {
            // do not update member_network_id
            unset($valid['member_network_id']);
            $purchase->update($valid);

            // Update purchase items
            $purchase->items()->delete(); // delete all first
            $purchase->items()->createMany($items); // create again

            // update purchase points
            $computePrevious->save();
            $computeCurrent->save();

            // Add in activity logs
            AdminLog::createLog('purchase_updated', $id, $purchase);

            DB::commit();
            return back();

        } catch (Throwable $e) {
            DB::rollback();
            return back()->withErrors(['message' => $e]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $purchase = Purchase::withMemberDetails()->findOrFail($id);

        $computePrevious = new MemberPurchPointComputeHelper($purchase->purchased_at, $purchase->member_network_id);
        // return error if payouted
        if ($computePrevious->isPayouted()) return back()->withErrors(['message' => "Points from this purchase already payouted. Cannot be deleted."]);

        DB::beginTransaction();
        try {
            $purchase->delete();
            $purchase->items()->delete();

            // update purchase points
            $computePrevious->save();

            // Add in activity logs
            AdminLog::createLog('purchase_deleted', $id, $purchase);

            DB::commit();
            return back();

        } catch (Throwable $e) {
            DB::rollback();
            return back()->withErrors(['message' => $e]);
        }
    }
}
