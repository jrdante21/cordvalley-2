<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;

use App\Helpers\MemberNetworkComputeHelper;
use App\Models\Member;
use App\Models\MemberNetworkPoint;

class DashboardController extends Controller
{
    public function __invoke()
    {
        $result = Member::withTotalOverallProfit()
            ->orderBy('total_profit', 'desc')
            ->having('total_profit', '>', 0)
            ->limit(20)
            ->get();
        
        return Inertia::render('Admin/Dashboard', [
            'result' => $result
        ]);
    }
}
