<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;

class SearchProductController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $result = Product::when($request->search, function($query, $search){
            $search = "%".$search."%";
            $query->where("title", "LIKE", $search);
        })
        ->limit(20)
        ->get();

        return $result;
    }
}
