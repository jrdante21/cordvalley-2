<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

use App\Http\Requests\MemberPayoutCreateRequest;
use App\Models\Member;
use App\Models\MemberPayout;
use App\Models\MemberPayoutItem;
use App\Models\MemberNetworkPoint;
use App\Models\AdminLog;

class PayoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $result = MemberPayout::with(['member'])
            ->withTotalProfit()
            ->when($request->search, function($query, $search){
                $query->whereHas('member', fn(Builder $q) => $q->searchByName($search));
            })
            ->when($request->pending,
                fn($query) => $query->whereNull('confirmed_at'),
                fn($query) => $query->with('admin')->confirmed()
            )
            ->orderBy('updated_at', 'desc')
            ->paginate($request->input('per_page', 20));
        
        return Inertia::render('Admin/Payouts/Index', [
            'result' => $result,
            'query' => $request->all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MemberPayoutCreateRequest $request)
    {
        $valid = $request->validated();
        $valid['confirmed_at'] = date('Y-m-d');
        $valid['admin_id'] = auth()->guard('admin')->id();

        DB::beginTransaction();
        try {
            $member = Member::withTotalProfit()->findOrFail($valid['member_id']);

            // Update profits
            $profits = MemberNetworkPoint::withProfitDetails()->whereIn('id', explode(',',$member->point_profit_ids))->get();
            DB::update("
                UPDATE member_network_points
                SET 
                    binary_points_payout = binary_points,
                    sponsor_points_payout = sponsor_points,
                    purchase_points_payout = purchase_points,
                    ordinal_points_payout = ordinal_points
                WHERE id IN ({$member->point_profit_ids})
            ");

            // Create payouts
            $payoutItems = collect($profits)->map(function($item){
                return [
                    'member_network_point_id' => $item->id,
                    'binary_points' => $item->binary_points_total,
                    'sponsor_points' => $item->sponsor_points_total,
                    'purchase_points' => $item->purchase_points_total,
                    'ordinal_points' =>  $item->ordinal_points_total
                ];
            })->all();
            $payout = MemberPayout::create($valid);
            $payout->items()->createMany($payoutItems);

            DB::commit();
            return back();

        } catch (Throwable $e) {
            DB::rollback();
            return back()->withErrors(['message' => $e]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MemberPayoutCreateRequest $request, $id)
    {
        $valid = $request->validated();
        $valid['confirmed_at'] = date('Y-m-d');
        $valid['admin_id'] = auth()->guard('admin')->id();

        $payout = MemberPayout::findOrFail($id);

        DB::beginTransaction();
        try {
            unset($valid['member_id']); // remove member_id cannot be changed
            $payout->update($valid);

            AdminLog::createLog('payout_updated', $id, $payout);

            DB::commit();
            return back();

        } catch (Throwable $e) {
            DB::rollback();
            return back()->withErrors(['message' => $e]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $payout = MemberPayout::with(['items','member'])->findOrFail($id);
        $hasLatest = MemberPayout::where('created_at', '>', $payout->created_at)->where('member_id', $payout->member_id)->count();

        // ignore delete pagnatabunan na ng ibang payout
        if ($hasLatest >= 1) {
            return back()->withErrors(['message' => "Payout cannot be deleted!"]);
        }
        
        DB::beginTransaction();
        try {
            $profits_ids = collect($payout->items)->pluck('member_network_point_id')->all();
            $profits_ids = implode(',', $profits_ids);

            // Reverse previous profit payout
            DB::update("
                UPDATE member_network_points AS points
                INNER JOIN member_payout_items AS payout ON points.id = payout.member_network_point_id AND payout.member_payout_id = {$payout->id}
                SET 
                    points.binary_points_payout = points.binary_points_payout - payout.binary_points,
                    points.sponsor_points_payout = points.sponsor_points_payout - payout.sponsor_points,
                    points.purchase_points_payout = points.purchase_points_payout - payout.purchase_points,
                    points.ordinal_points_payout = points.ordinal_points_payout - payout.ordinal_points
                WHERE points.id IN ({$profits_ids})
            ");

            $payout->delete();
            $payout->items()->delete();

            AdminLog::createLog('payout_deleted', $id, $payout);

            DB::commit();
            return back();

        } catch (Throwable $e) {
            DB::rollback();
            return back()->withErrors(['message' => $e]);
        }
    }
}
