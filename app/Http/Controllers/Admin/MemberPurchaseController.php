<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;

use App\Models\Member;
use App\Models\Purchase;

class MemberPurchaseController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke($id, Request $request)
    {
        $member = Member::with('networks')->findOrFail($id);

        $result = Purchase::with(['admin','items'])
            ->withMemberDetails()
            ->when($request->search, fn($query, $search) => $query->searchUsingView($search))
            ->where('member_id', $id)
            ->orderBy('purchases.created_at', 'desc')
            ->paginate($request->input('per_page', 20));

        // Add urls
        $result->getCollection()->transform(function ($item) {
            $item->total_amount = collect($item->items)->sum(fn($i) => $i->total_amount);
            $item->total_points = collect($item->items)->sum(fn($i) => $i->total_points);
            return $item;
        });
        
        return Inertia::render('Admin/Members/MemberPurchase', [
            'member' => $member,
            'result' => $result,
            'query' => $request->all()
        ]);
    }
}
