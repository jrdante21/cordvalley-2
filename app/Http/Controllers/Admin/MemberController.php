<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Inertia\Inertia;

use App\Http\Requests\MemberRequest;
use App\Models\Member;
use App\Models\MemberNetwork;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $result = Member::withTotalNetworkings()
            ->searchByName($request->search)
            ->whereAddress($request)
            ->latest()
            ->paginate($request->input('per_page', 20));
        
        return Inertia::render('Admin/Members/Index', [
            'result' => $result,
            'query' => $request->all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MemberRequest $request)
    {
        $valid = $request->validated();
        $valid['account_id'] = Member::generateAccountID();
        $valid['password']= Hash::make($valid['password']);

        $member = Member::create($valid);
        $memberId = $member->id;
        return to_route('admin.members.show', $memberId);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $member = Member::withTotalOverallProfit()->findOrFail($id);

        // get networkngs
        $networks = MemberNetwork::with('code')
            ->withGenealogyDetails()
            ->where('member_networks.member_id', $id)
            ->groupBy('member_networks.id')
            ->get();

        return Inertia::render('Admin/Members/MemberShow', [
            'member' => $member,
            'networks' => $networks,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MemberRequest $request, $id)
    {
        $valid = $request->validated();
        if (!empty($request->edit_login)) {
            $valid['password'] = Hash::make($valid['password']);
        } else {
            unset($valid['password']);
            unset($valid['username']);
        }

        Member::where('id', $id)->update($valid);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
