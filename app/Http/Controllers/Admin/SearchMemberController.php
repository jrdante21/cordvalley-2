<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Member;

class SearchMemberController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $result = Member::searchByName($request->search)
            ->when($request->with_profit, fn($query) => $query->withTotalProfit())
            ->limit(20)
            ->get();

        return $result;
    }
}
