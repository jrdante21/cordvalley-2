<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;

use App\Http\Requests\ProductRequest;
use App\Models\Product;
use App\Models\PurchaseItem;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $result = Product::with('admin')
            ->when($request->search, function($query, $search){
                $query->where('title', 'LIKE', '%'.$search.'%')
                    ->orWhere('abbreviation', 'LIKE', '%'.$search.'%');
            })
            ->latest()
            ->paginate($request->input('per_page', 20));

        return Inertia::render('Admin/Products/Index', [
            'result' => $result,
            'query' => $request->all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $valid = $request->validated();
        $valid['admin_id'] = auth()->guard('admin')->id();
        Product::create($valid);
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        $valid = $request->validated();
        Product::where('id',$id)->update($valid);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $purchased_count = PurchaseItem::where('product_id', $id)->count();
        if ($purchased_count <= 0) {
            $product = Product::find($id);
            $product->delete();
            return back();

        } else {
            return back()->withErrors([ 'message' => "Product was used in purchase. It can't be deleted." ]);
        }
    }
}
