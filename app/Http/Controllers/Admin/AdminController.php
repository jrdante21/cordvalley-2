<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Inertia\Inertia;

use App\Http\Requests\AdminRequest;
use App\Http\Requests\AdminMainRequest;
use App\Models\Admin;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $result = Admin::searchByName($request->search)
            ->whereNotIn('id', [1,2])
            ->latest()
            ->paginate($request->input('per_page', 20));
        
        return Inertia::render('Admin/Admin', [
            'result' => $result,
            'query' => $request->all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminRequest $request)
    {
        $valid = $request->validated();
        $valid['is_super'] = 0;
        $valid['password']= Hash::make($valid['password']);

        Admin::create($valid);
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminRequest $request, $id)
    {
        $valid = $request->validated();
        if (!empty($request->edit_login)) {
            $valid['password'] = Hash::make($valid['password']);
        } else {
            unset($valid['password']);
            unset($valid['username']);
        }

        Admin::where('id', $id)->update($valid);
        return back();
    }

    // Update main or current login account
    public function update_main(AdminMainRequest $request)
    {
        $id = auth()->guard('admin')->id();

        $valid = $request->validated();
        if (!empty($request->edit_login)) {
            $valid['password'] = Hash::make($valid['password']);
        } else {
            unset($valid['password']);
            unset($valid['username']);
        }

        unset($valid['password_old']);
        Admin::where('id', $id)->update($valid);
        return back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin = Admin::findOrFail($id);
        $admin->delete();
        return back();
    }
}
