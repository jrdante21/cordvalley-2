<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Helpers\MemberProfitSummaryHelper;
use App\Models\Member;
use App\Models\MemberNetwork;
use App\Models\Purchase;

class DashboardController extends Controller
{
    public function index()
    {
        $id = auth()->id();
        $member = Member::withTotalOverallProfit()->Find($id);

        // get networkngs
        $networks = MemberNetwork::with('code')
            ->withGenealogyDetails()
            ->where('member_networks.member_id', $id)
            ->groupBy('member_networks.id')
            ->get();

        return Inertia::render('Member/Dashboard', [
            'member' => $member,
            'networks' => $networks,
        ]);
    }

    public function purchases(Request $request)
    {
        $id = auth()->id();

        $result = Purchase::with(['admin','items'])
            ->withMemberDetails()
            ->when($request->search, fn($query, $search) => $query->searchUsingView($search))
            ->where('member_id', $id)
            ->orderBy('purchases.created_at', 'desc')
            ->paginate($request->input('per_page', 20));

        // Add urls
        $result->getCollection()->transform(function ($item) {
            $item->total_amount = collect($item->items)->sum(fn($i) => $i->total_amount);
            $item->total_points = collect($item->items)->sum(fn($i) => $i->total_points);
            return $item;
        });
        
        return Inertia::render('/Member/Purchases', [
            'result' => $result,
            'query' => $request->all()
        ]);
    }

    public function summary(Request $request)
    {
        $id = auth()->id();

        $result = new MemberProfitSummaryHelper($id);
        $result = $result->summarize($request->input('per_page', 20));
        return Inertia::render('/Member/Summary', [
            'result' => $result,
            'query' => $request->all()
        ]);
    }
}
