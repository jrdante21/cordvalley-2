<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:web')->except(['logout']);
    }

    public function index()
    {
        return Inertia::render('Member/Login');
    }

    public function login(Request $request)
    {
        $valid = $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        if (auth()->attempt($valid)) return redirect()->route('member.dashboard');

        return back()->withErrors(['message' => "Username or password is incorrect."]);
    }

    public function logout()
    {
        auth()->logout();
        return redirect()->route('member.login');
    }
}
