<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MemberNetwork;
use App\Helpers\MemberNetworkComputeHelper;

class MemberNetworkDownlineController extends Controller
{
    public function __invoke(Request $request)
    {
        $id = $request->network_id;
        $compute = new MemberNetworkComputeHelper();

        $downlines = MemberNetwork::select('*')
            ->with('member')
            ->whereByUplines($id)
            ->orWhere('member_networks.id',$id)
            ->withGenealogyDetails()
            // ->withCheckStatus()
            ->withLatestLegCount()
            ->groupBy('member_networks.id')
            ->orderByRaw('CHAR_LENGTH(member_networks.binary_nodes)')
            ->limit($request->input('limit', 20))
            ->get()
            ->transform(function($item) use ($compute){
                $computed_legs = $compute->__computePairings(
                    $item->lft_count_prev, 
                    $item->rgt_count_prev, 
                    $item->lft_count_curr, 
                    $item->rgt_count_curr
                );
                $item->paired_vs = max(0, $computed_legs['lft_count'] - $computed_legs['rgt_count']) ." vs ". max(0, $computed_legs['rgt_count'] - $computed_legs['lft_count']);
                return $item;
            });

        $head = collect($downlines)->where('id', $id)->first();

        return [
            'head' => $head,
            'downlines' => $downlines
        ];
    }
}
