<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MemberPayoutCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'member_id' => ['required', 'numeric', 'exists:members,id'],
            'remarks' => 'required|min:5',
            'total_profit' => 'required|numeric|min:1000',
            'requested_at' => ['required', 'before_or_equal:'.date('Y-m-d')],
        ];
    }

    public function attributes()
    {
        return [
            'requested_at' => 'date request',
        ];
    }
}
