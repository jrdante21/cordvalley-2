<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PurchaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */    
    public function rules()
    {
        return [
            'member_network_id' => 'required|numeric|exists:member_networks,id',
            'remarks' => 'nullable|min:5',
            'ar_number' => 'required|min:3',
            'purchased_at' => 'required|date|before_or_equal:'.date('Y-m-d'),
            'items' => 'required|array',
            'items.*.product_id' => 'required|numeric|exists:products,id',
            'items.*.price' => 'required|numeric',
            'items.*.points' => 'required|numeric',
            'items.*.quantity' => 'required|numeric',
        ];
    }

    public function attributes()
    {
        return [
            'purchased_at' => 'date purchased',
            'items.*.product_id' => 'product',
            'items.*.price' => 'price',
            'items.*.points' => 'points',
            'items.*.quantity' => 'quantity',
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'purchased_at' => date('Y-m-d', strtotime($this->purchased_at)),
        ]);
    }
}
