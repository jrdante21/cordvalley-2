<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Rules\AlphaSpace;

class AdminMainRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        $id = auth()->guard('admin')->id();

        return [
            'fname' => ['required', 'min:2', new AlphaSpace],
            'mname' => ['required', 'min:2', new AlphaSpace],
            'lname' => ['required', 'min:2', new AlphaSpace],
            'username' => ['required_if_accepted:edit_login', 'nullable', 'alpha_num', 'min:5', Rule::unique('members', 'username')->ignore($id)],
            'password' => ['required_if_accepted:edit_login', 'confirmed', 'nullable', 'min:5'],
            'password_old' => ['required_if_accepted:edit_login', 'current_password:admin', 'nullable', 'min:5'],
        ];
    }

    public function attributes()
    {
        return [
            'fname' => 'first name',
            'mname' => 'middle name',
            'lname' => 'last name',
            'password_old' => 'old password',
        ];
    }

    public function messages()
    {
        return [
            'username.required_if_accepted' => "The username is required.",
            'password.required_if_accepted' => "The password is required.",
            'password_old.required_if_accepted' => "The old password is required.",
        ];
    }
}
