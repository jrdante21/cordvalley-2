<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $id = $this->route('product');

        return [
            'title' => ['required', 'min:5', Rule::unique('products', 'title')->ignore($id)],
            'abbreviation' => ['required', 'min:2', Rule::unique('products', 'abbreviation')->ignore($id)],
            'description' => 'required|min:5',
            'price' => 'required|numeric|min:1',
            'points' => 'required|numeric|min:1',
        ];
    }
}
