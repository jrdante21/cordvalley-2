<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Rules\AlphaSpace;

class MemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $id = $this->route('member');

        return [
            'fname' => ['required', 'min:2', new AlphaSpace],
            'mname' => ['required', 'min:2', new AlphaSpace],
            'lname' => ['required', 'min:2', new AlphaSpace],
            'suffix' => ['nullable', 'min:2', new AlphaSpace],
            'contact_number' => ['nullable', 'numeric'],
            'gender' => ['nullable', 'numeric'],
            'bday' => ['required', 'before_or_equal:'.date('Y-12-31', strtotime('-15 years'))],
            'barangay' => ['nullable', 'min:2'],
            'municipality' => ['nullable', 'min:2'],
            'province' => ['nullable', 'min:2'],
            'username' => ['required_if_accepted:edit_login', 'nullable', 'alpha_num', 'min:5', Rule::unique('members', 'username')->ignore($id)],
            'password' => ['required_if_accepted:edit_login', 'nullable', 'min:5']
        ];
    }

    public function attributes()
    {
        return [
            'fname' => 'first name',
            'mname' => 'middle name',
            'lname' => 'last name',
            'bday' => 'birthdate'
        ];
    }

    public function messages()
    {
        return [
            'bday.before_or_equal' => "The birthdate is invalid.",
            'username.required_if_accepted' => "The username is required.",
            'password.required_if_accepted' => "The password is required.",
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'bday' => (!empty($this->bday)) ? date('Y-m-d', strtotime($this->bday)) : null,
            'barangay' => (!empty($this->barangay)) ? strtoupper($this->barangay) : null,
            'municipality' => (!empty($this->municipality)) ? strtoupper($this->municipality) : null,
            'province' => (!empty($this->province)) ?strtoupper($this->province) : null,
        ]);
    }
}
