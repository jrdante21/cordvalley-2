<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Rules\AlphaSpace;

class MemberNetworkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'member_id' => ['required', 'numeric', 'exists:members,id'],
            'code' => ['required'],
            'username' => ['required', 'alpha_num', 'min:5', Rule::unique('member_networks', 'username')],
            'sponsored_by' => ['exclude_if:no_sponsor,1', 'required'],
            'placed_in' => ['exclude_if:no_sponsor,1', 'required'],
            'placement' => ['exclude_if:no_sponsor,1', 'required', Rule::in([1,2])],
        ];
    }

    public function attributes()
    {
        return [
            'sponsored_by' => "sponsor's networking username",
            'placed_in' => "placement's networking username",
        ];
    }
}
