<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('admins')->delete();
        
        \DB::table('admins')->insert(array (
            0 => 
            array (
                'created_at' => NULL,
                'deleted_at' => NULL,
                'fname' => 'Super',
                'is_super' => 1,
                'lname' => 'Admin',
                'mname' => 'Over',
                'password' => '$2y$10$i5ZUqffZio6eaT6JiiqcT.BAtY/VE2UPM0ZxFOSRFpxgDWgGXx6d2',
                'updated_at' => NULL,
                'username' => 'admin',
            ),
            1 => 
            array (
                'created_at' => NULL,
                'deleted_at' => NULL,
                'fname' => 'Dalandan',
                'is_super' => 2,
                'lname' => 'Admin',
                'mname' => 'Over',
                'password' => '$2y$10$i5ZUqffZio6eaT6JiiqcT.BAtY/VE2UPM0ZxFOSRFpxgDWgGXx6d2',
                'updated_at' => NULL,
                'username' => 'dalandan',
            ),
        ));
    }
}
