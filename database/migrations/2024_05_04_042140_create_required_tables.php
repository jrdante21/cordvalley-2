<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // settings
        Schema::create('settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('keyname', 100)->unique('keyname');
            $table->json('keyvalue');
        });

        // Admins
        Schema::create('admins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username', 255)->unique('username');
            $table->string('password', 255);
            $table->string('fname', 255);
            $table->string('mname', 255);
            $table->string('lname', 255);
            $table->boolean('is_super');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('admin_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('admin_id');
            $table->string('keyword', 255);
            $table->unsignedBigInteger('keyword_id');
            $table->json('previous_data');
            $table->timestamps();
        });

        // Codes
        Schema::create('codes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('admin_id');
            $table->string('remarks', 500);
            $table->timestamps();
        });
        Schema::create('code_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('member_network_id')->nullable();
            $table->unsignedBigInteger('code_id');
            $table->string('code', 100)->unique('code');
            $table->dateTime('used_at');
        });

        // Members
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('account_id', 20)->unique('account_id');
            $table->string('username', 255)->unique('username');
            $table->string('password', 255);
            $table->string('fname', 255);
            $table->string('mname', 255)->nullable();
            $table->string('lname', 255);
            $table->string('suffix', 5)->nullable();
            $table->unsignedTinyInteger('gender')->nullable();
            $table->date('bday');
            $table->string('contact_number', 20)->nullable();
            $table->string('barangay', 255)->nullable();
            $table->string('municipality', 255)->nullable();
            $table->string('province', 255)->nullable();
            $table->timestamps();
        });

        // Member networking
        Schema::create('member_networks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('member_id');
            $table->string('username', 255)->unique('username');
            $table->unsignedBigInteger('placed_in')->nullable();
            $table->unsignedBigInteger('sponsored_by')->nullable();
            $table->unsignedBigInteger('lft_leg')->nullable();
            $table->unsignedBigInteger('rgt_leg')->nullable();
            $table->text('binary_nodes');
            $table->text('sponsor_nodes');
            $table->tinyInteger('type');
            $table->timestamps();
        });
        Schema::create('member_network_points', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('member_network_id');
            $table->integer('lft_count');
            $table->integer('rgt_count');
            $table->integer('pair_count');
            $table->integer('sponsor_count');
            $table->decimal('binary_points', 10, 2)->nullable();
            $table->decimal('sponsor_points', 10, 2)->nullable();
            $table->decimal('ordinal_points', 10, 2)->nullable();
            $table->decimal('purchase_points', 10, 2)->nullable();
            $table->decimal('binary_points_payout', 10, 2)->nullable();
            $table->decimal('sponsor_points_payout', 10, 2)->nullable();
            $table->decimal('ordinal_points_payout', 10, 2)->nullable();
            $table->decimal('purchase_points_payout', 10, 2)->nullable();
            $table->tinyInteger('cycle');
            $table->tinyInteger('type');
            $table->date('date');
        });
        Schema::create('member_network_histories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('member_network_id');
            $table->unsignedBigInteger('admin_id');
            $table->text('remarks');
            $table->timestamps();
        });

        // Member payout
        Schema::create('member_payouts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('member_id');
            $table->text('remarks');
            $table->unsignedBigInteger('admin_id')->nullable();
            $table->date('confirmed_at')->nullable();
            $table->date('requested_at')->nullable();
            $table->timestamps();
        });
        Schema::create('member_payout_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('member_payout_id');
            $table->unsignedBigInteger('member_network_point_id');
            $table->decimal('binary_points', 10, 2);
            $table->decimal('sponsor_points', 10, 2);
            $table->decimal('ordinal_points', 10, 2);
            $table->decimal('purchase_points', 10, 2);
        });

        // Products
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('admin_id');
            $table->string('title', 255);
            $table->string('abbreviation', 50);
            $table->string('image', 255)->nullable();
            $table->text('description')->nullable();
            $table->decimal('price', 10, 2);
            $table->decimal('price_discounted', 10, 2)->nullable();
            $table->decimal('points', 10, 2);
            $table->timestamps();
        });

        // Purchases
        Schema::create('purchases', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('member_network_id');
            $table->unsignedBigInteger('admin_id');
            $table->text('remarks')->nullable();
            $table->string('ar_number', 20);
            $table->date('purchased_at');
            $table->timestamps();
        });
        Schema::create('purchase_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('purchase_id');
            $table->unsignedBigInteger('product_id');
            $table->decimal('price', 10, 2);
            $table->decimal('points', 10, 2);
            $table->integer('quantity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
        Schema::dropIfExists('admins');
        Schema::dropIfExists('code_items');
        Schema::dropIfExists('members');
        Schema::dropIfExists('member_networks');
        Schema::dropIfExists('member_network_points');
        Schema::dropIfExists('member_network_histories');
        Schema::dropIfExists('member_payouts');
        Schema::dropIfExists('member_profits');
        Schema::dropIfExists('products');
        Schema::dropIfExists('product_items');
        Schema::dropIfExists('purchases');
        Schema::dropIfExists('purchase_items');
    }
};
