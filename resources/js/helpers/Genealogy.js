export default class Genealogy {
    createTree(container, network, downlines) {
        this.networkDownlines = downlines

        const tree = this.#setTree(network)
        container.appendChild(tree)
    }

    #findInDownlines(id) {
        return this.networkDownlines.find(el => el.id == id)
    }

    #createCard(title, subtitle, details, tooltip = '', addClass ='text-bg-info') {
        const card = document.createElement('div')
        card.classList.add('card','mx-auto','text-center',addClass)

        const cardBody = document.createElement('div')
        cardBody.classList.add('card-body')

        const cardTitle = document.createElement('h5')
        cardTitle.classList.add('card-title')
        cardTitle.innerText = title

        const cardSubtitle = document.createElement('div')
        cardSubtitle.classList.add('card-subtitle')
        cardSubtitle.innerText = subtitle

        const cardDetails = document.createElement('p')
        cardDetails.classList.add('card-text')
        cardDetails.innerHTML = details

        cardBody.appendChild(cardTitle)
        cardBody.appendChild(cardSubtitle)
        cardBody.appendChild(cardDetails)
        card.appendChild(cardBody)
        card.setAttribute('title', tooltip)

        return card
    }

    #createViewMoreButton(id=0) {
        const button = document.createElement('button')
        button.classList.add('btn','btn-success','btn-view-more','mx-auto','py-3','py-2')
        button.innerText = 'View More'

        const onClick = this.onClickViewMore
        button.addEventListener('click', function(event){
            let btn = event.target
            let parent = btn.parentElement
            onClick(id, parent)
            btn.remove()
        })
        return button
    }

    #setTree(headNetwork) {
        if (!headNetwork || !headNetwork?.username) {
            return this.#createCard('None', '---', 'No networking account', '', 'text-bg-secondary')
        }

        const { lft_leg, rgt_leg } = headNetwork
        const lft = this.#findInDownlines(lft_leg)
        const rgt = this.#findInDownlines(rgt_leg)

        // create card for header
        const status = (headNetwork.status >= 1) ? 'text-bg-info' : 'text-bg-danger'
        const cardHead = this.#createCard(headNetwork.username, headNetwork.member?.fullname ?? '---', headNetwork.registered, headNetwork.paired_vs)

        const lftHtml = (!lft && lft_leg >= 1) ? this.#createViewMoreButton(lft_leg) : this.#setTree(lft)
        const rgtHtml = (!rgt && rgt_leg >= 1) ? this.#createViewMoreButton(rgt_leg) : this.#setTree(rgt)
        
        // Use bootstrap classes
        // header
        const tdHeadLeg = document.createElement('div')
        tdHeadLeg.classList.add('genealogy-item-leg')

        const tdHead = document.createElement('td')
        tdHead.setAttribute('colspan', 2)
        tdHead.setAttribute('width', '100%')
        tdHead.appendChild(cardHead)
        tdHead.appendChild(tdHeadLeg)

        const trHead = document.createElement('tr')
        trHead.appendChild(tdHead)

        // legs
        const lefLft = document.createElement('div')
        const lefRgt = document.createElement('div')
        lefLft.classList.add('genealogy-item-leg-lft')
        lefRgt.classList.add('genealogy-item-leg-rgt')

        const tdLft = document.createElement('td')
        tdLft.setAttribute('width', '50%')
        tdLft.appendChild(lefLft)
        tdLft.appendChild(lftHtml)

        const tdRgt = document.createElement('td')
        tdRgt.setAttribute('width', '50%')
        tdRgt.appendChild(lefRgt)
        tdRgt.appendChild(rgtHtml)

        const trLegs = document.createElement('tr')
        trLegs.appendChild(tdLft)
        trLegs.appendChild(tdRgt)
        
        // main table
        const table = document.createElement('table')
        table.appendChild(trHead)
        table.appendChild(trLegs)

        return table
    }
}
