import '@/bootstrap';
import { createApp, h } from 'vue'
import { createInertiaApp, Link } from '@inertiajs/vue3'
import PrimeVue from 'primevue/config'
import Ripple from 'primevue/ripple'
import { ZiggyVue } from 'ziggy-js'

// Custom Components
import LayoutAdmin from '@/components/Admin/Layout.vue'
import CustomLoader from '@/components/Loader.vue'
import CustomForm from '@/components/CustomForm.vue'
import InvalidFeedback from '@/components/InvalidFeedback.vue'
import FieldSelectDropdown from '@/components/FieldSelectDropdown.vue'

// Primevue components
import Button from 'primevue/button'
import SplitButton from 'primevue/splitbutton'
import InputGroup from 'primevue/inputgroup'
import InputMask from 'primevue/inputmask'
import InputNumber from 'primevue/inputnumber'
import InputText from 'primevue/inputtext'
import InputSwitch from 'primevue/inputswitch'
import Password from 'primevue/password'
import Textarea from 'primevue/textarea'
import DataTable from 'primevue/datatable'
import Column from 'primevue/column'
import ColumnGroup from 'primevue/columngroup'  // optional
import Row from 'primevue/row'                   // optional
import Card from 'primevue/card'
import Menu from 'primevue/menu'
import Menubar from 'primevue/menubar'
import Paginator from 'primevue/paginator'
import Message from 'primevue/message'
import Calendar from 'primevue/calendar'
import Dropdown from 'primevue/dropdown'
import Dialog from 'primevue/dialog'
import Badge from 'primevue/badge'


// Setup inertia
createInertiaApp({
  resolve: name => {
    const pages = import.meta.glob('./pages/**/*.vue', { eager: true })
    return pages[`./pages/${name}.vue`]
  },
  setup({ el, App, props, plugin }) {
    createApp({ render: () => h(App, props) })
      .component('LayoutAdmin', LayoutAdmin)
      .component('CustomLoader', CustomLoader)
      .component('CustomForm', CustomForm)
      .component('InvalidFeedback', InvalidFeedback)
      .component('FieldSelectDropdown', FieldSelectDropdown)

      .component('Button', Button)
      .component('SplitButton', SplitButton)
      .component('InputGroup', InputGroup)
      .component('InputMask', InputMask)
      .component('InputNumber', InputNumber)
      .component('InputText', InputText)
      .component('InputSwitch', InputSwitch)
      .component('Password', Password)
      .component('Textarea', Textarea)
      .component('DataTable', DataTable)
      .component('Column', Column)
      .component('ColumnGroup', ColumnGroup)
      .component('Row', Row)
      .component('Card', Card)
      .component('Menu', Menu)
      .component('Menubar', Menubar)
      .component('Paginator', Paginator)
      .component('Message', Message)
      .component('Calendar', Calendar)
      .component('Dropdown', Dropdown)
      .component('Dialog', Dialog)
      .component('Badge', Badge)

      .component('InertiaLink', Link)
      .directive('ripple', Ripple)
      .use(plugin)
      .use(ZiggyVue)
      .use(PrimeVue, { 
        ripple: true
      })
      .mount(el)
  },
})
