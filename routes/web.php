<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\DashboardController as AdminDashboard;
use App\Http\Controllers\Admin\MemberController as AdminMember;
use App\Http\Controllers\Admin\MemberNetworkController as AdminMemberNetwrok;
use App\Http\Controllers\Admin\MemberPurchaseController as AdminMemberPurchase;
use App\Http\Controllers\Admin\MemberPayoutController as AdminMemberPayout;
use App\Http\Controllers\Admin\MemberSummaryController as AdminMemberSummary;
use App\Http\Controllers\Admin\CodeController as AdminCode;
use App\Http\Controllers\Admin\ProductController as AdminProduct;
use App\Http\Controllers\Admin\PurchaseController as AdminPurchase;
use App\Http\Controllers\Admin\PayoutController as AdminPayout;
use App\Http\Controllers\Admin\LoginController as AdminLogin;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\SearchMembersNetworkController;
use App\Http\Controllers\Admin\SearchMemberController;
use App\Http\Controllers\Admin\SearchProductController;

use App\Http\Controllers\MemberNetworkDownlineController;
use App\Http\Controllers\Member\LoginController as MemberLogin;
use App\Http\Controllers\Member\DashboardController as MemberDashboard;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::inertia('/', 'Home/Index')->name('home');
// Route::get('/members/{id}/summary', AdminMemberSummary::class)->name('member_summary');

// Ajax only
Route::middleware('ajax')->prefix('capt-ajax')->group(function(){

    // Admins and members
    Route::middleware('auth:admin,web')->group(function(){
        Route::get('/networking_genealogy', MemberNetworkDownlineController::class)->name('networking_genealogy');
    });
});

// Members
Route::get('/login', [MemberLogin::class, 'index'])->name('member.login');
Route::name('member.')->prefix('capt-member')->group(function(){
    Route::post('/login', [MemberLogin::class, 'login'])->name('login_post');
    Route::get('/logout', [MemberLogin::class, 'logout'])->name('logout');

    Route::middleware('auth:web')->group(function(){
        Route::get('/dashboard', [MemberDashboard::class, 'index'])->name('dashboard');
        Route::get('/purchases', [MemberDashboard::class, 'purchases'])->name('purchases');
        Route::get('/summary', [MemberDashboard::class, 'summary'])->name('summary');
    });
});

// Only admin
Route::name('admin.')->prefix('capt-admin')->group(function(){
    Route::get('/', [AdminLogin::class, 'index'])->name('login');
    Route::post('/login', [AdminLogin::class, 'login'])->name('login_post');
    Route::get('/logout', [AdminLogin::class, 'logout'])->name('logout');

    Route::middleware('auth:admin')->group(function(){
        Route::get('/dashboard', AdminDashboard::class)->name('dashboard');
    
        Route::resource('members', AdminMember::class);
        Route::resource('members.networks', AdminMemberNetwrok::class);
        Route::get('/members/{id}/purchases', AdminMemberPurchase::class)->name('member_purchases');
        Route::get('/members/{id}/payouts', AdminMemberPayout::class)->name('member_payouts');
        Route::get('/members/{id}/summary', AdminMemberSummary::class)->name('member_summary');
    
        Route::resource('products', AdminProduct::class);
        Route::resource('purchases', AdminPurchase::class);
        Route::resource('payouts', AdminPayout::class);

        // for super admin only
        Route::resource('codes', AdminCode::class)->middleware('is_super');
        Route::resource('admins', AdminController::class)->middleware('is_super');

        // Update main admin account
        Route::put('/admin-main-update', [AdminController::class, 'update_main'])->name('admin_main_update');
    
        // Search ajax
        Route::middleware('ajax')->group(function(){
            Route::get('/search-members', SearchMemberController::class)->name('search_members');
            Route::get('/search-members-network', SearchMembersNetworkController::class)->name('search_member_networks');
            Route::get('/search-products', SearchProductController::class)->name('search_products');
        });
    });
});